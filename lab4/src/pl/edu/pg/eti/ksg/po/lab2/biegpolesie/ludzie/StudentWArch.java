package pl.edu.pg.eti.ksg.po.lab2.biegpolesie.ludzie;

import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.DziedzinaZadania;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.KierunekStudiow;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.Zadanie;

public class StudentWArch extends StudentPolitechniki {
    public StudentWArch(String imie, String nazwisko, Plec plec, KierunekStudiow kierunek) {
        super(imie, nazwisko, plec, kierunek);
    }

    @Override
    public void przedstawSie() {
        wypowiedzSie("Studiuję na WArch");
    }

    @Override
    public boolean rozwiazZadanie(Zadanie zadanie) {
        if (zadanie.getDziedzinaZadania() == DziedzinaZadania.SZTUKA) {
            return Math.random() * 100 <= 70;
        }
        return super.rozwiazZadanie(zadanie);
    }
}
