package pl.edu.pg.eti.ksg.po.lab2.biegpolesie.roboty;

import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.DziedzinaZadania;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.Robot;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.RodzajTerenu;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.Zadanie;

public class Terminator extends Robot {
    public Terminator(String model, int numerSeryjny) {
        super(model, numerSeryjny);
    }

    @Override
    public double predkoscPoruszaniaSie(RodzajTerenu rodzajTerenu) {
        switch(rodzajTerenu)
        {
            case DROGA:
                return czynnikiLosowe.nextDouble()*0.2 + 0.9; //Od 0.8 do 1.0
            case SCIEZKA:
                return czynnikiLosowe.nextDouble()*0.2 + 0.7; //Od 0.6 do 0.8
            case WYSOKI_LAS:
                return czynnikiLosowe.nextDouble()*0.2 + 0.5; //Od 0.4 do 0.6
            case NISKI_LAS:
                return czynnikiLosowe.nextDouble()*0.2 + 0.3; //Od 0.2 do 0.4
            case BAGNO:
            default:
                return czynnikiLosowe.nextDouble()*0.3; //Od 0 do 0.2
        }
    }

    @Override
    public boolean rozwiazZadanie(Zadanie zadanie) {
        DziedzinaZadania dziedzinaZadania = zadanie.getDziedzinaZadania();
        if (dziedzinaZadania == DziedzinaZadania.SZTUKA || dziedzinaZadania == DziedzinaZadania.NAUKI_LESNE) {
            return super.rozwiazZadanie(zadanie);
        }
        return true;
    }
}
