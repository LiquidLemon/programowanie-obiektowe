package pl.edu.pg.eti.ksg.po.lab2.biegpolesie.ludzie;

import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.DziedzinaZadania;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.KierunekStudiow;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.Zadanie;

public class StudentPolitechniki extends Student {
    public StudentPolitechniki(String imie, String nazwisko, Plec plec, KierunekStudiow kierunek) {
        super(imie, nazwisko, plec, kierunek);
    }

    @Override
    public void przedstawSie() {
       wypowiedzSie("Studiuję " + kierunekStudiow + " na politechnice");
    }

    @Override
    public boolean rozwiazZadanie(Zadanie zadanie) {
        DziedzinaZadania dziedzina = zadanie.getDziedzinaZadania();
        if (dziedzina == DziedzinaZadania.FIZYKA) {
            return Math.random() * 2 <= 1;
        }
        if (dziedzina == DziedzinaZadania.MATEMATYKA || dziedzina == DziedzinaZadania.INFORMATYKA) {
            return Math.random() * 100 <= 70;
        }
        return super.rozwiazZadanie(zadanie);
    }
}
