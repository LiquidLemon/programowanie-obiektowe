package pl.edu.pg.eti.ksg.po.lab2.biegpolesie.ludzie;

import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.DziedzinaZadania;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.KierunekStudiow;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.Zadanie;

public class StudentWETI extends StudentPolitechniki {
    public StudentWETI(String imie, String nazwisko, Plec plec, KierunekStudiow kierunek) {
        super(imie, nazwisko, plec, kierunek);
    }

    @Override
    public void przedstawSie() {
        wypowiedzSie("Studiuję " + kierunekStudiow + " na WETI");
    }

    @Override
    public boolean rozwiazZadanie(Zadanie zadanie) {
        if (zadanie.getDziedzinaZadania() == DziedzinaZadania.INFORMATYKA) {
           return Math.random() * 100 < 99;
        }
        return super.rozwiazZadanie(zadanie);
    }
}
