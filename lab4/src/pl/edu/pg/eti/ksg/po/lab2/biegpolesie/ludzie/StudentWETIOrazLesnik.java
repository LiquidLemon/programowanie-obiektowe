package pl.edu.pg.eti.ksg.po.lab2.biegpolesie.ludzie;

import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.DziedzinaZadania;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.KierunekStudiow;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.RodzajTerenu;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.Zadanie;

public class StudentWETIOrazLesnik extends StudentWETI {
    public StudentWETIOrazLesnik(String imie, String nazwisko, Plec plec, KierunekStudiow kierunek) {
        super(imie, nazwisko, plec, kierunek);
    }

    @Override
    public void przedstawSie() {
        wypowiedzSie("Jestem studentem i leśnikiem");
    }

    @Override
    public boolean rozwiazZadanie(Zadanie zadanie) {
        if (zadanie.getDziedzinaZadania() == DziedzinaZadania.NAUKI_LESNE) {
            return Math.random() * 100 <= 70;
        }
        return super.rozwiazZadanie(zadanie);
    }

    @Override
    public double predkoscPoruszaniaSie(RodzajTerenu rodzajTerenu) {
        if (rodzajTerenu == RodzajTerenu.NISKI_LAS) {
            return humorIUwarunkowaniaOsobiste.nextDouble() * 0.2 + 0.5;
        }
        return super.predkoscPoruszaniaSie(rodzajTerenu);
    }
}
