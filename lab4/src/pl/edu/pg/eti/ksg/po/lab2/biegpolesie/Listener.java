package pl.edu.pg.eti.ksg.po.lab2.biegpolesie;

public interface Listener {
    public void handle(Wydarzenie wydarzenie);
}
