package pl.edu.pg.eti.ksg.po.lab2.biegpolesie.roboty;

import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.DziedzinaZadania;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.RodzajTerenu;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.Zadanie;

public class TerminatorL extends Terminator {
    public TerminatorL(String model, int numerSeryjny) {
        super(model, numerSeryjny);
    }

    @Override
    public double predkoscPoruszaniaSie(RodzajTerenu rodzajTerenu) {
        switch(rodzajTerenu)
        {
            case NISKI_LAS:
                return czynnikiLosowe.nextDouble()*0.3 + 0.4; //Od 0.2 do 0.4
            default:
                return super.predkoscPoruszaniaSie(rodzajTerenu);
        }
    }

    @Override
    public boolean rozwiazZadanie(Zadanie zadanie) {
        DziedzinaZadania dziedzinaZadania = zadanie.getDziedzinaZadania();
        if (dziedzinaZadania == DziedzinaZadania.NAUKI_LESNE) {
            return Math.random() * 100 <= 60;
        }
        return true;
    }
}
