package pl.edu.pg.eti.ksg.po.lab2.biegpolesie;

public class WydarzenieWejsciaNaMete extends Wydarzenie {
    private final Uczestnik uczestnik;

    public WydarzenieWejsciaNaMete(Uczestnik uczestnik) {
       this.uczestnik = uczestnik;
       rodzajWydarzenia = RodzajWydarzenia.WEJSCIE_NA_METE;
    }

    public Uczestnik getUczestnik() {
        return uczestnik;
    }
}
