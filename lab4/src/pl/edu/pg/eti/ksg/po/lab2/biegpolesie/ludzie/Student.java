package pl.edu.pg.eti.ksg.po.lab2.biegpolesie.ludzie;

import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.Czlowiek;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.KierunekStudiow;
import pl.edu.pg.eti.ksg.po.lab2.biegpolesie.Zadanie;

public class Student extends Czlowiek {
    protected final KierunekStudiow kierunekStudiow;
    public Student(String imie, String nazwisko, Plec plec, KierunekStudiow kierunek) {
        super(imie, nazwisko, plec);
        kierunekStudiow = kierunek;
    }

    @Override
    public void przedstawSie() {
        wypowiedzSie("Jestem studentem kierunku " + kierunekStudiow);
    }

    @Override
    public boolean rozwiazZadanie(Zadanie zadanie) {
        return Math.random() * 5 <= 1;
    }
}
