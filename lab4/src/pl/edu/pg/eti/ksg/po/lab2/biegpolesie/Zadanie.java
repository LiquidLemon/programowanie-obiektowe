package pl.edu.pg.eti.ksg.po.lab2.biegpolesie;

import java.util.HashSet;
import java.util.Set;

public class Zadanie implements ElementTrasy {
    private final DziedzinaZadania dziedzinaZadania;
    private final Set<Uczestnik> uczestnicy = new HashSet<>();

    Zadanie(DziedzinaZadania dziedzinaZadania) {
        this.dziedzinaZadania = dziedzinaZadania;
    }

    @Override
    public Iterable<Uczestnik> getUczestnicy() {
        return uczestnicy;
    }

    @Override
    public void dodajUczestnika(Uczestnik u) {
        uczestnicy.add(u);
    }

    @Override
    public void usunUczestnika(Uczestnik u) {
        uczestnicy.remove(u);
    }

    @Override
    public int getLiczbaUczestnikowNaTrasie() {
        return uczestnicy.size();
    }

    public DziedzinaZadania getDziedzinaZadania() {
        return dziedzinaZadania;
    }
}
