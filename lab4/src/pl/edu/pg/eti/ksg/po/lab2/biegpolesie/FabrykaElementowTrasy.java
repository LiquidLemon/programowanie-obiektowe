package pl.edu.pg.eti.ksg.po.lab2.biegpolesie;

public class FabrykaElementowTrasy {
    public static ElementTrasy utworzElementTrasy(RodzajTerenu rodzajTerenu) {
        return new Teren(rodzajTerenu);
    }

    public static ElementTrasy utworzElementTrasy(DziedzinaZadania dziedzinaZadania) {
        return new Zadanie(dziedzinaZadania);
    }

    public static ElementTrasy utworzElementTrasy(String name) throws NieznanyElementTrasyException {
        try {
            RodzajTerenu rodzajTerenu = RodzajTerenu.valueOf(name);
            return new Teren(rodzajTerenu);
        } catch (IllegalArgumentException e) {
            // pass
        }

        try {
            DziedzinaZadania dziedzinaZadania = DziedzinaZadania.valueOf(name);
            return new Zadanie(dziedzinaZadania);
        } catch (IllegalArgumentException e) {
            // pass
        }

        throw new NieznanyElementTrasyException();

    }
}
