#include "Kolo.hpp"

#include <cmath>

Kolo::Kolo(double r)
  : r(r)
{

}

Kolo::~Kolo() {
  std::cout << "~Kolo(): " << *this << std::endl;
}

double Kolo::GetR() const {
  return r;
}

void Kolo::SetR(double value) {
  r = value;
}

void Kolo::Wypisz(std::ostream& out) const {
  out << "<Kolo r: " << r << ">";
}

double Kolo::Pole() const {
  return M_PI * r * r;
}

double Kolo::Obwod() const {
  return 2 * M_PI * r ;
}
