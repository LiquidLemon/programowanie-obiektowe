#include <cassert>
#include <cmath>
#include <array>

#include "Trojkat.hpp"
#include "Prostokat.hpp"
#include "Kolo.hpp"

#include "util.hpp"

int main() {
  Prostokat p(2, 3);
  assert(p.GetA() == 2);
  assert(p.GetB() == 3);

  p.SetA(5);
  assert(p.GetA() == 5);

  p.SetB(6);
  assert(p.GetB() == 6);

  f_assert_eq(p.Obwod(), 22);
  f_assert_eq(p.Pole(), 30);

  Trojkat *t = new Trojkat(1, 2, 3);
  assert(t->GetA() == 1);
  assert(t->GetB() == 2);
  assert(t->GetC() == 3);

  t->SetA(3);
  assert(t->GetA() == 3);

  t->SetB(4);
  assert(t->GetB() == 4);

  t->SetC(5);
  assert(t->GetC() == 5);

  f_assert_eq(t->Obwod(), 12);
  f_assert_eq(t->Pole(), 6);

  Kolo k(5);

  assert(k.GetR() == 5);

  k.SetR(3);
  assert(k.GetR() == 3);

  f_assert_eq(k.Obwod(), 6 * M_PI);
  f_assert_eq(k.Pole(), 9 * M_PI);

  std::array<FiguraPlaska*, 3> figury = { &p, t, &k };

  for (auto figura : figury) {
    std::cout << *figura << std::endl;
    std::cout << "Obwod: " << figura->Obwod() << std::endl;
    std::cout << "Pole: " << figura->Pole() << std::endl;
  }

  delete t;
  return 0;
}
