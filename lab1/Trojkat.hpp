#pragma once
#include "FiguraPlaska.hpp"

class Trojkat : public FiguraPlaska {
  private:
    double a, b, c;
  protected:
    void Wypisz(std::ostream& out) const override;
  public:
    Trojkat(double a, double b, double c);

    double GetA() const;
    void SetA(double value);
    double GetB() const;
    void SetB(double value);
    double GetC() const;
    void SetC(double value);

    double Obwod() const override;
    double Pole() const override;

    ~Trojkat() override;
};
