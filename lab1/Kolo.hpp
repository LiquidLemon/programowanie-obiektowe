#pragma once

#include "FiguraPlaska.hpp"
#include <iostream>

class Kolo : public FiguraPlaska {
  private:
    double r;
  protected:
    void Wypisz(std::ostream& out) const override;
  public:
    Kolo(double r);
    ~Kolo() override;

    double Pole() const override;
    double Obwod() const override;

    double GetR() const;
    void SetR(double value);
};
