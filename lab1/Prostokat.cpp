#include "Prostokat.hpp"

#include <iostream>

Prostokat::Prostokat(double a, double b) :
  a(a),
  b(b)
{

}

double Prostokat::GetA() const {
  return a;
}

void Prostokat::SetA(double value) {
  a = value;
}

double Prostokat::GetB() const {
  return b;
}

void Prostokat::SetB(double value) {
  b = value;
}

double Prostokat::Obwod() const {
  return 2 * (a + b);
}

double Prostokat::Pole() const {
  return a * b;
}

void Prostokat::Wypisz(std::ostream& out) const {
  out << "<Prostokat a:" << a << " b: " << b << ">";
  out << std::endl << this->Obwod();
}

Prostokat::~Prostokat() {
  std::cout << "~Prostokat(): " << *this << std::endl;
}
