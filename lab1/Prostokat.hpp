#include "FiguraPlaska.hpp"

class Prostokat : public FiguraPlaska {
private:
  double a, b;
protected:
  void Wypisz(std::ostream& out) const override;
public:
  Prostokat(double a, double b);

  // Accessors
  double GetA() const;
  void SetA(double value);
  double GetB() const;
  void SetB(double value);

  double Obwod() const override;
  double Pole() const override;

  ~Prostokat() override;
};
