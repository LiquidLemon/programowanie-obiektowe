#include "Trojkat.hpp"

#include <iostream>
#include <cmath>

Trojkat::Trojkat(double a, double b, double c) :
  a(a),
  b(b),
  c(c)
{

}

void Trojkat::Wypisz(std::ostream& out) const {
  out << "<Trojkat a: " << a << " b: " << b << " c: " << c << ">";
}

double Trojkat::GetA() const {
  return a;
}

void Trojkat::SetA(double value) {
  a = value;
}

double Trojkat::GetB() const {
  return b;
}

void Trojkat::SetB(double value) {
  b = value;
}

double Trojkat::GetC() const {
  return c;
}

void Trojkat::SetC(double value) {
  c = value;
}


double Trojkat::Obwod() const {
  return a + b + c;
}

double Trojkat::Pole() const {
  double p = Obwod() / 2;
  return sqrt(p * (p - a) * (p - b) * (p - c));
}

Trojkat::~Trojkat() {
  std::cout << "~Trojkat(): " << *this << std::endl;
}
