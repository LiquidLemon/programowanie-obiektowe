#define F_COMPARISON_ACCURACY 0.00001

#define f_assert_eq(left, right) \
  assert(abs(left - right) < F_COMPARISON_ACCURACY)

