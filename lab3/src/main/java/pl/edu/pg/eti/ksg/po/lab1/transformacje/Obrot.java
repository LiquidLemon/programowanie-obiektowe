package pl.edu.pg.eti.ksg.po.lab1.transformacje;

public class Obrot implements Transformacja {
  private final double angle;

  public Obrot(double angle) {
    this.angle = angle;
  }

  @Override
  public Transformacja getTransformacjaOdwrotna() {
    return new Obrot(-angle);
  }

  @Override
  public Punkt transformuj(Punkt p) {
    double x = p.getX() * Math.cos(angle) - p.getY() * Math.sin(angle);
    double y = p.getX() * Math.sin(angle) + p.getY() * Math.cos(angle);
    return new Punkt(x, y);
  }

  @Override
  public String toString() {
    return "Obrot(" + angle + ")";
  }
}
