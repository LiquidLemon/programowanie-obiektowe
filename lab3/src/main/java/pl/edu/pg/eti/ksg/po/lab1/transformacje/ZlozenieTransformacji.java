package pl.edu.pg.eti.ksg.po.lab1.transformacje;

import java.util.Arrays;

public class ZlozenieTransformacji implements Transformacja {
  private final Transformacja[] transforms;

  public ZlozenieTransformacji(Transformacja[] transforms) {
    this.transforms = transforms;
  }

  @Override
  public Transformacja getTransformacjaOdwrotna() throws
    BrakTransformacjiOdwrotnejException
  {
    Transformacja[] transformacjeOdwrotne = new Transformacja[transforms.length];
    for (int i = 0; i < transforms.length; i++) {
      transformacjeOdwrotne[i] = transforms[transforms.length - 1 - i].getTransformacjaOdwrotna();
    }

    return new ZlozenieTransformacji(transformacjeOdwrotne);
  }

  @Override
  public Punkt transformuj(Punkt p) {
    for (Transformacja t : transforms) {
      p = t.transformuj(p);
    }
    return p;
  }
}
