package pl.edu.pg.eti.ksg.po.lab1;

import pl.edu.pg.eti.ksg.po.lab1.transformacje.*;
class JavaLab1 {
  public static void main(String[] args) {
    try {
      Punkt p1 = Punkt.E_X;
      System.out.println(p1);

      Transformacja tr = new Translacja(5, 6);
      System.out.println(tr);

      Punkt p2 = tr.transformuj(p1);
      System.out.println(p2);

      Transformacja trr = tr.getTransformacjaOdwrotna();
      System.out.println(trr);

      Punkt p3 = trr.transformuj(p2);
      System.out.println(p3);
    } catch (BrakTransformacjiOdwrotnejException ex) {
      ex.printStackTrace();
    }
    System.out.println();

    try {
      Punkt p1 = new Punkt(2, 2);
      System.out.println(p1);

      Transformacja tr = new Skalowanie(5, 4);
      System.out.println(tr);

      Punkt p2 = tr.transformuj(p1);
      System.out.println(p2);

      Transformacja trr = tr.getTransformacjaOdwrotna();
      System.out.println(trr);

      Punkt p3 = trr.transformuj(p2);
      System.out.println(p3);
    } catch (BrakTransformacjiOdwrotnejException ex) {
      ex.printStackTrace();
    }
    System.out.println();

    try {
      Punkt p1 = new Punkt(2, 2);
      System.out.println(p1);

      Transformacja tr = new Skalowanie(5, 0);
      System.out.println(tr);

      Punkt p2 = tr.transformuj(p1);
      System.out.println(p2);

      Transformacja trr = tr.getTransformacjaOdwrotna();
      System.out.println(trr);

      Punkt p3 = trr.transformuj(p2);
      System.out.println(p3);
    } catch (BrakTransformacjiOdwrotnejException ex) {
      ex.printStackTrace();
    }
    System.out.println();

    try {
      Punkt p1 = new Punkt(2, 3);
      System.out.println(p1);

      Transformacja tr = new Obrot(3);
      System.out.println(tr);

      Punkt p2 = tr.transformuj(p1);
      System.out.println(p2);

      Transformacja trr = tr.getTransformacjaOdwrotna();
      System.out.println(trr);

      Punkt p3 = trr.transformuj(p2);
      System.out.println(p3);
    } catch (BrakTransformacjiOdwrotnejException ex) {
      ex.printStackTrace();
    }
    System.out.println();

    try {
      Punkt p1 = new Punkt(1, 1);
      System.out.println(p1);

      Transformacja tr = new ZlozenieTransformacji(new Transformacja[] {
        new Translacja(2, 3),
        new Skalowanie(3, 2)
      });
      System.out.println(tr);

      Punkt p2 = tr.transformuj(p1);
      System.out.println(p2);

      Transformacja trr = tr.getTransformacjaOdwrotna();
      System.out.println(trr);

      Punkt p3 = trr.transformuj(p2);
      System.out.println(p3);
    } catch (BrakTransformacjiOdwrotnejException ex) {
      ex.printStackTrace();
    }
  }
}
