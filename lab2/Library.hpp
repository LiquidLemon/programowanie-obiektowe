#pragma once

#include <initializer_list>
#include <ostream>

#include "Book.hpp"

class Library {
public:
  Library();
  Library(std::size_t size);
  Library(std::initializer_list<Book> list);
  Library(const Library& other);
  Library(Library&& other);
  ~Library();

  Library& operator=(const Library& other);
  Library& operator=(Library&& other);

  Book& operator[](std::size_t index);
  const Book& operator[](std::size_t index) const;
  std::size_t GetSize() const;

private:
  std::size_t size;
  Book *books;
};

std::ostream& operator<<(std::ostream& out, Library& library);
