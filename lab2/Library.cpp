#include "Library.hpp"
#include "util.hpp"
#include <exception>

Library::Library() :
  size(0),
  books(nullptr)
{
  libraryLogger << "Library::Library()";
}

Library::Library(std::size_t size) :
  size(size),
  books(new Book[size])
{
  libraryLogger << "Library::Library(std::size_t)";
}

Library::Library(std::initializer_list<Book> list) :
  size(list.size()),
  books(new Book[size])
{
  libraryLogger << "Library::Library(std::initializer_list<Book>)";
  std::size_t i = 0;
  for (Book book : list) {
    books[i++] = book;
  }
}

Library::Library(const Library& other) :
  size(other.size),
  books(new Book[size])
{
  libraryLogger << "Library::Library(const Library&)";
  for (std::size_t i = 0; i < size; i++) {
    books[i] = other.books[i];
  }
}

Library::Library(Library&& other) :
  size(other.size),
  books(other.books)
{
  libraryLogger << "Library::Library(Library&&)";
  other.size = 0;
  other.books = nullptr;
}

Library::~Library() {
  libraryLogger << "Library::~Library()";
  if (books != nullptr) {
    delete[] books;
  }
}

Library& Library::operator=(const Library& other) {
  libraryLogger << "Library::operator=(const Library&)";
  delete[] books;
  size = other.size;
  books = new Book[size];
  for (std::size_t i = 0; i < size; i++) {
    books[i] = other.books[i];
  }

  return *this;
}

Library& Library::operator=(Library&& other) {
  libraryLogger << "Library::operator=(Library&&)";
  books = other.books;
  size = other.size;

  other.books = nullptr;
  other.size = 0;

  return *this;
}

Book& Library::operator[](std::size_t index) {
  libraryLogger << "Library::operator[](std::size_t)";
  if (index >= size) {
    throw std::length_error("Library index out of bounds");
  }
  return books[index];
}

const Book& Library::operator[](std::size_t index) const {
  libraryLogger << "const Library::operator[](std::size_t) const";
  return (*this)[index];
}

std::size_t Library::GetSize() const {
  return size;
}

std::ostream& operator<<(std::ostream& out, Library& library) {
  for (auto i = 0; i < library.GetSize(); i++) {
    out << library[i] << std::endl;
  }
  return out;
}
