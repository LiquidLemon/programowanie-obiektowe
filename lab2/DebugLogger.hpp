#pragma once
#include <ostream>

class DebugLogger {
  std::ostream& stream;
  bool enabled = true;
public:
  DebugLogger(std::ostream& stream) : stream(stream) {}
  void enable() { enabled = true; }
  void disable() { enabled = false; }
  void toggle() { enabled = !enabled; }
  DebugLogger& operator<<(const std::string message) {
    if (enabled) {
      stream << message << std::endl;
    }
    return *this;
  }
};
