#include <iostream>
#include "util.hpp"
#include "Book.hpp"
#include "Library.hpp"

DebugLogger bookLogger(std::cout);
DebugLogger libraryLogger(std::cout);

void testBook() {
  std::string a = "John Flanagan", t = "Ranger's Apprentice";
  Book e;
  std::cout << "e: " << e << std::endl;

  Book b1 = {a, t};
  std::cout << "b1: " << b1 << std::endl;

  Book b2 = {"Donald Knuth", "The Art of Programming"};
  std::cout << "b2: " << b2 << std::endl;

  Book b3 = b1;

  std::cout << "b3: " << b3 << std::endl << "b1: " << b1 << std::endl;

  e = std::move(b2);
  std::cout << "e: " << e << std::endl << "b2: " << b2 << std::endl;
  e.SetAuthor("Andrzej Sapkowski");
  std::cout << "e: " << e << std::endl;
  e.SetTitle("Miecz przeznaczenia");
  std::cout << "e: " << e << std::endl;
}

void testLibrary() {
  Library e;
  std::cout << "e: " << e << std::endl;

  Library l1 = {
    {"Andrew S. Tanenbaum", "Modern Operating Systems"},
    {"H. P. Lovecraft", "Call of Cthulhu"},
    {"Timothy Zahn", "Outbound Flight"}
  };
  std::cout << "l1: " << std::endl << l1 << std::endl;

  Library l2(2);
  std::cout << "l2:" << std::endl << l2 << std::endl;
  l2[0] = {"George Orwell", "1984"};
  l2[1] = {"Terry Pratchett", "The Colour of Magic"};
  std::cout << "l2:" << std::endl << l2 << std::endl;

  e = std::move(l2);
  std::cout << "e:" << std::endl << e << std::endl;
  std::cout << "l2:" << std::endl << l2 << std::endl;
}

int main() {
  std::cout << " --- Book --- " << std::endl;
  testBook();
  bookLogger.disable();
  std::cout << std::endl << " --- Library --- " << std::endl;
  testLibrary();
}
