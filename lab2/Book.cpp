#include "Book.hpp"

#include "util.hpp"

Book::Book() :
  author(""),
  title("")
{
  bookLogger << "Book::Book()";
}

Book::Book(const std::string& author, const std::string& title) :
  author(author),
  title(title)
{
  bookLogger << "Book::Book(const std::string&, const std::string&)";
}

Book::Book(std::string&& author, std::string&& title) :
  author(std::move(author)),
  title(std::move(title))
{
  bookLogger << "Book::Book(std::string&&, const std::string&&)";
}

Book::Book(const Book& other) :
  author(other.author),
  title(other.title)
{
  bookLogger << "Book::Book(const Book&)";
}

Book::Book(Book&& other) :
  author(std::move(other.author)),
  title(std::move(other.title))
{
  bookLogger << "Book::Book(Book&&)";
}

Book& Book::operator=(const Book& other) {
  bookLogger << "Book::operator=(const Book&)";
  author = other.author;
  title = other.title;
  return *this;
}

Book& Book::operator=(Book&& other) {
  bookLogger << "Book::operator=(Book&&)";
  author = std::move(other.author);
  title = std::move(other.title);
  return *this;
}

std::string Book::GetAuthor() const {
  return author;
}

void Book::SetAuthor(const std::string& value) {
  bookLogger << "Book::SetAuthor(const std::string&)";
  author = value;
}

void Book::SetAuthor(std::string&& value) {
  bookLogger << "Book::SetAuthor(std::string&&)";
  author = std::move(value);
}

std::string Book::GetTitle() const {
  return title;
}

void Book::SetTitle(const std::string& value) {
  bookLogger << "Book::SetTitle(const std::string&)";
  title = value;
}

void Book::SetTitle(std::string&& value) {
  bookLogger << "Book::SetTitle(std::string&&)";
  title = std::move(value);
}

std::ostream& operator<<(std::ostream& out, Book& book) {
  out << book.GetAuthor() << ": \"" << book.GetTitle() << "\"";
  return out;
}
