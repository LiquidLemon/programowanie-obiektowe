#pragma once

#include <string>
#include <ostream>

class Book {
  std::string author;
  std::string title;
public:
  Book();
  Book(const std::string& author, const std::string& title);
  Book(std::string&& author, std::string&& title);

  Book(const Book& other);
  Book(Book&& other);

  Book& operator=(const Book& other);
  Book& operator=(Book&& other);

  std::string GetAuthor() const;
  void SetAuthor(const std::string& value);
  void SetAuthor(std::string&& value);

  std::string GetTitle() const;
  void SetTitle(const std::string& value);
  void SetTitle(std::string&& value);
};

std::ostream& operator<<(std::ostream& out, Book& book);
